#pragma once

#include<iostream>

class Matrix2D {
private:
    double** matrix;
    int size;

public:
    Matrix2D() {}
    Matrix2D(int size): size(size) {
        matrix = new double* [size];

        for(int i = 0; i < size; i++) {
            matrix[i] = new double[size];

            for(int j = 0; j < size; j++) {
                matrix[i][j] = 0.0;
            } 
        }
    }

    double& operator()(int i, int j) {
        return matrix[i][j];
    }

    void printToScreen() {
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                std::cout << matrix[i][j] << " ";
            } 

            std::cout << std::endl;
        }

        std::cout << std::endl;
    }
};