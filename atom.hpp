#pragma once

#include<string>
#include<cmath>

class Atom {
public:
    int atomicNumber;
    double x;
    double y;
    double z;

    Atom(int Z, double x, double y, double z) {
        this->atomicNumber = Z;
        this->x = x;
        this->y = y;
        this->z = z;
    }

    double distanceTo(Atom* other) {
        return sqrt(pow(this->x - other->x,2 ) + pow(this->y - other->y, 2) + pow(this->z - other->z, 2));
    }

    std::string toString() {
        return std::to_string(atomicNumber) + " " + std::to_string(x) + "\t" + std::to_string(y) + "\t" + std::to_string(z);
    }
};