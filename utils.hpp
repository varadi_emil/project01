#pragma once

#include <cmath>

#include "matrix_2d.hpp"

namespace Utils {
    class CartesianVector {
    private:
        double vector[3];
    public:
        CartesianVector() {
            vector[0] = 0.0;
            vector[1] = 0.0;
            vector[2] = 0.0;
        }

        CartesianVector(double x, double y, double z) {
            vector[0] = x;
            vector[1] = y;
            vector[2] = z;
        }

        double& operator[](int index) {
            return vector[index];
        }
    };

    CartesianVector crossProduct(CartesianVector a, CartesianVector b) {
        double cx = a[1]*b[2] - a[2]*b[1];
        double cy = a[2]*b[0] - a[0]*b[2];
        double cz = a[0]*b[1] - a[1]*b[0];

        CartesianVector result(cx, cy, cz);
        return result; 
    }

    double dotProduct(CartesianVector a, CartesianVector b) {
        return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
    }

    double mapAtomicNumberToAtomicMass(int Z) {
        switch (Z) {
        case 3:
            return 6.9;
        case 4:
            return 9.0;
        case 5:
            return 10.8;
        default:
            return Z*2.0;
        }
    }

    /// Computes the determinant for the specific case of an inertia tensor
    /// and solves the cubic equation numerically.
    CartesianVector eigenValuesFromInertia(Matrix2D inertia) {
        /// labeling elements in determinant
        double a = inertia(0, 0);
        double b = inertia(1, 1);
        double c = inertia(2, 2);

        double d = inertia(0, 1);
        double e = inertia(0, 2);
        double f = inertia(1, 2);

        /// elements of the cubic equation
        double A = -1.0;
        double B = c+b+a;
        double C = -b*c - a*c - a*b + e*e + f*f + d*d;
        double D = a*b*c + 2.0*d*f*e - e*e*b - f*f*a - d*d*c;

        /// solving the cubic equations
        double D0 = B*B - 3.0*A*C;
        double D1 = 2.0*B*B*B - 9.0*A*B*C + 27.0*A*A*D;

        double CCplus = cbrt((D1 + sqrt(std::max(0.0, D1*D1 - 4.0*D0*D0*D0)))/2.0);
        double CCminus = cbrt((D1 - sqrt(std::max(0.0, D1*D1 - 4.0*D0*D0*D0)))/2.0);

        double CC;
        if(CCplus <= 0.001 && CCplus >= -0.001) {
            CC = CCplus;
        }
        else {
            CC = CCminus;
        }

        double xi = (-1.0 + sqrt(3.0))/2.0;

        /// the solutions are the eigenvalues
        CartesianVector eigenValues;
        for(int k = 0; k < 2; k++) {
            eigenValues[k] = -(1.0/3.0*A)*(B + pow(xi, (double)k) * CC + D0/(pow(xi, (double) k) * CC));
        }

        return eigenValues;
    }
}