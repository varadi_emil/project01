#include<iostream>

#include "file_reader.hpp"
#include "atom.hpp"
#include "molecule.hpp"

int main(int argc, char* argv[]) {
    FileReader* reader = new FileReader("input.txt");

    std::vector<Atom>& atoms = reader->getAtoms();
    int numberOfAtoms = reader->getNumberOfAtoms();

    Molecule* molecule = new Molecule(atoms);
    molecule->printEverything();

    delete reader;
    delete molecule;
    return 0;
}