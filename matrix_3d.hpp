#pragma once

#include<iostream>

class Matrix3D {
private:
    double*** matrix;
    int size;

public:
    Matrix3D() {}
    Matrix3D(int size):size(size) {
        matrix = new double** [size];

        for(int i = 0; i < size; i++) {
            matrix[i] = new double*[size];

            for(int j = 0; j < size; j++) {
                matrix[i][j] = new double[size];

                for(int k = 0; k < size; k++) {
                    matrix[i][j][k] = 0.0;
                }
            }
        }
    }

    double& operator()(int i, int j, int k) {
        return matrix[i][j][k];
    }

    void printToScreen() {
        for(int k = 0; k < size; k++) {
            for(int i = 0; i < size; i++) {
                for(int j = 0; j < size; j++) {
                   std::cout << matrix[i][j][k] << " "; 
                }
            }

            std::cout << std::endl;
        }

        std::cout << std::endl;
    }
};