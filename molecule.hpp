#pragma once

#include<vector>
#include<cmath>

#include "atom.hpp"
#include "matrix_2d.hpp"
#include "matrix_3d.hpp"
#include "matrix_4d.hpp"
#include "utils.hpp"

class Molecule{
private:
    std::vector<Atom> atoms;
    Matrix2D R;
    Matrix2D ex;
    Matrix2D ey;
    Matrix2D ez;

    Matrix3D fi;

    Matrix4D theta;
    Matrix4D tau;

    Utils::CartesianVector cm;

    Matrix2D inertia;

    double Ia;
    double Ib;
    double Ic;

    double rotA;
    double rotB;
    double rotC;

    void calculateDistances() {
        for(int i = 0; i < atoms.size() - 1; i++) {
            for(int j = i + 1; j < atoms.size(); j++) {
                double r = atoms.at(i).distanceTo(&atoms.at(j));
                R(i, j) = r;
                R(j, i) = r;
            }
        }
    }

    void calculateUnitVectors() {
        for(int i = 0; i < atoms.size() - 1; i++) {
            for(int j = i + 1; j < atoms.size(); j++) {
                ex(i, j) = - (atoms.at(i).x - atoms.at(j).x)/R(i, j);
                ex(j, i) = -ex(i ,j);

                ey(i, j) = - (atoms.at(i).y - atoms.at(j).y)/R(i, j);
                ey(j, i) = -ey(i ,j);

                ez(i, j) = - (atoms.at(i).z - atoms.at(j).z)/R(i, j);
                ez(j, i) = -ez(i ,j);
            }
        }
    }

    void calculateBondAngles() {
        for(int i = 0; i < atoms.size(); i++) {
            for(int j = 0; j < atoms.size(); j++) {
                for(int k = 0; k < atoms.size(); k++) {
                    if(i == j || j == k) {
                        continue;
                    }

                    fi(i, j, k) = acos(unitVectorScalarProduct(j, i, j, k));
                }
            }
        }
    }

    void calculateOutOfPlaneAngles() {
        for(int i = 0; i < atoms.size(); i++) {
            for(int j = 0; j < atoms.size(); j++) {
                for(int k = 0; k < atoms.size(); k++) {
                    for(int l = 0; l < atoms.size(); l++) {
                        if(j == k || k == l || j== l) {
                            continue;
                        }

                        Utils::CartesianVector a(ex(k, j), ey(k, j), ez(k, j)); 
                        Utils::CartesianVector b(ex(k, l), ey(k, l), ez(k, l)); 
                        Utils::CartesianVector cross = Utils::crossProduct(a, b);

                        Utils::CartesianVector c(ex(k, i), ey(k, i), ez(k, i));
                        double scalar = Utils::dotProduct(cross, c);
                        double sinTheta = scalar/sin(fi(j, k, l));

                        if(sinTheta <= -1.0) {
                            sinTheta = -0.999;
                        }
                        if(sinTheta >= 1.0) {
                            sinTheta = 0.999;
                        }

                        theta(i, j, k, l) = asin(sinTheta);
                    }
                }
            }
        }
    }

    void calculateTorsionAngles() {
        for(int i = 0; i < atoms.size(); i++) {
            for(int j = 0; j < atoms.size(); j++) {
                for(int k = 0; k < atoms.size(); k++) {
                    for(int l = 0; l < atoms.size(); l++) {
                        if(i == j || j == k || k == i || j == l || k == l) {
                            continue;
                        }

                        Utils::CartesianVector a(ex(i, j), ey(i, j), ez(i, j));
                        Utils::CartesianVector b(ex(j, k), ey(j, k), ez(j, k));
                        Utils::CartesianVector cross1 = Utils::crossProduct(a, b);

                        Utils::CartesianVector c(ex(j, k), ey(j, k), ez(j, k));
                        Utils::CartesianVector d(ex(k, l), ey(k, l), ez(k, l));
                        Utils::CartesianVector cross2 = Utils::crossProduct(c, d);

                        double scalar = Utils::dotProduct(cross1, cross2);
                        double cosine = scalar/(sin(fi(i, j, k)) * sin(fi(j, k, l)));

                        if(cosine <= -1.0) {
                            cosine = -0.999;
                        }
                        if(cosine >= 1.0) {
                            cosine = 0.999;
                        }

                        tau(i, j, k, l) = acos(cosine);
                    }
                }
            }
        }
    }

    void calculateCenterOfMass() {
        double totalMass = 0.0;

        for(int i = 0; i < atoms.size(); i++) {
            totalMass += Utils::mapAtomicNumberToAtomicMass(atoms.at(i).atomicNumber);

            cm[0] += atoms.at(i).x * Utils::mapAtomicNumberToAtomicMass(atoms.at(i).atomicNumber);
            cm[1] += atoms.at(i).y * Utils::mapAtomicNumberToAtomicMass(atoms.at(i).atomicNumber);
            cm[2] += atoms.at(i).z * Utils::mapAtomicNumberToAtomicMass(atoms.at(i).atomicNumber);
        }

        cm[0] /= totalMass;
        cm[1] /= totalMass;
        cm[2] /= totalMass;
    }

    void calculateInertia() {
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                double inert = 0.0;
                for(int a = 0; a < atoms.size(); a++) {
                    double mass = Utils::mapAtomicNumberToAtomicMass(atoms.at(a).atomicNumber);
                    double x = atoms.at(a).x;
                    double y = atoms.at(a).y;
                    double z = atoms.at(a).z;

                    if(i == 0 && j == 0) {
                        inert += mass*(y*y + z*z);
                    }

                    if(i == 1 && j == 1) {
                        inert += mass*(x*x + z*z);
                    }

                    if(i == 2 && j == 2) {
                        inert += mass*(x*x + y*y);
                    }

                    if((i == 0 && j == 1) || (i == 1 && j == 0)) {
                        inert += -mass*(x*y);
                    }

                    if((i == 0 && j == 2) || (i == 2 && j == 0)) {
                        inert += -mass*(x*z);
                    }

                    if((i == 1 && j == 2) || (i == 2 && j == 1)) {
                        inert += -mass*(y*z);
                    }
                }

                inertia(i, j) = inert;
            }
        }
    }

    void calculatePrincipalInertia() {
        Utils::CartesianVector eigenValues = Utils::eigenValuesFromInertia(inertia);

        /// sorting the eigenvalues in ascending order Ia <= Ib <= Ic
        Ia = eigenValues[0];

        for(int i = 1; i < 2; i++) {
            if(eigenValues[i] <= Ia) {
                Ia = eigenValues[i];
            }
        }

        Ic = eigenValues[0];

        for(int i = 1; i < 2; i++) {
            if(eigenValues[i] >= Ic) {
                Ic = eigenValues[i];
            }
        }

        Ib = eigenValues[0];

        for(int i = 1; i < 2; i++) {
            if(eigenValues[i] != Ia && eigenValues[i] != Ic) {
                Ib = eigenValues[i];
            }
        }
    }

    void calculateRotationalConstants() {
        double h = 6.62e-34;
        double c = 3.0e8;
        double m0 = 1.6e-27;
        double r0 = 5.3e-11;

        rotA = h/(8.0*M_PI*M_PI*c*Ia*m0*r0*r0);
        rotB = h/(8.0*M_PI*M_PI*c*Ib*m0*r0*r0);
        rotC = h/(8.0*M_PI*M_PI*c*Ic*m0*r0*r0);
    }

    double unitVectorScalarProduct(int i, int j, int k, int l) {
        return ex(i, j)*ex(k, l) + ey(i, j)*ey(k, l) + ez(i, j)*ez(k, l);
    }

public:
    Molecule(std::vector<Atom> atoms) {
        this->atoms = atoms;

        R = Matrix2D(atoms.size());
        ex = Matrix2D(atoms.size());
        ey = Matrix2D(atoms.size());
        ez = Matrix2D(atoms.size());
        fi = Matrix3D(atoms.size());
        theta = Matrix4D(atoms.size());
        tau = Matrix4D(atoms.size());
        cm = Utils::CartesianVector();
        inertia = Matrix2D(3);

        calculateDistances();
        calculateUnitVectors();
        calculateBondAngles();
        calculateOutOfPlaneAngles();
        calculateTorsionAngles();
        calculateCenterOfMass();
        calculateInertia();
        calculatePrincipalInertia();
        calculateRotationalConstants();
    }

    void printEverything() {
        std::cout << "ATOMS:" << std::endl;

        for(int i = 0 ; i < atoms.size(); i++) {
            std::cout << atoms.at(i).toString() << std::endl;
        }
        std::cout << std::endl;

        std::cout << "DISTANCES: " << std::endl;
        R.printToScreen();

        std::cout << "ex: " << std::endl;
        ex.printToScreen();

        std::cout << "ey: " << std::endl;
        ey.printToScreen();

        std::cout << "ez: " << std::endl;
        ez.printToScreen();


        std::cout << "fi: " << std::endl;
        fi.printToScreen();

        std::cout << "theta: " << std::endl;
        theta.printToScreen();

        std::cout << "tau: " << std::endl;
        tau.printToScreen();

        std::cout << "CENTER OF MASS: " << std::endl;
        std::cout << cm[0] << " " << cm[1] << " " << cm[2] << std::endl;

        std::cout << "INERTIA: " << std::endl;
        inertia.printToScreen();

        std::cout << "PRINCIPLE MOMENTS: " << std::endl;
        std::cout << Ia << " " << Ib << " " << Ic << std::endl;

        std::cout << "ROTATIONAL CONSTANTS (in m^-1): " << std::endl;
        std::cout << std::scientific;
        std::cout << rotA << " " << rotB << " " << rotC << std::endl;
    }
};