#pragma once

#include<iostream>

class Matrix4D {
private:
    double**** matrix;
    int size;
public:
    Matrix4D() {}
    Matrix4D(int size): size(size) {
        matrix = new double*** [size];

        for(int i = 0; i < size; i++) {
            matrix[i] = new double**[size];

            for(int j = 0; j < size; j++) {
                matrix[i][j] = new double*[size];

                for(int k = 0; k < size; k++) {
                    matrix[i][j][k] = new double[size];

                    for(int l = 0; l < size; l++) {
                        matrix[i][j][k][l] = 0.0;
                    }
                }
            }
        }
    }

    double& operator()(int i, int j, int k, int l) {
        return matrix[i][j][k][l];
    }

    void printToScreen() {
        for(int l =0; l < size; l++) {
            for(int k = 0; k < size; k++) {
                for(int i = 0; i < size; i++) {
                    for(int j = 0; j < size; j++) {
                    std::cout << matrix[i][j][k][l] << " "; 
                    }
                }

                std::cout << std::endl;
            }

            std::cout << std::endl;
        }

        std::cout << std::endl;
    }
};