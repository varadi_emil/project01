
CC=g++

TARGET=main
HDRS:=$(wildcard *.hpp)
$(TARGET): $(TARGET).cpp $(HDRS)
	$(CC) $^ -o $@ -lm

clean:
	@rm -fv main.o main