#pragma once

#include<iostream>
#include<fstream>
#include<string>
#include<vector>

#include "atom.hpp"

class FileReader {
private:
    std::string fileName;
    std::ifstream* input;
    std::vector<Atom> atoms;

    int numberOfAtoms;

    void readNumberOfAtoms() {
        if(!(*input >> numberOfAtoms)) {
            std::cout << "Error while reading first line!\n";
        }
    }

    void readAtoms() {
        int Z;
        double x,y,z;

        for(int i = 0; i < numberOfAtoms; i++) {
            if(! (*input >> Z >> x >> y >> z)) {
                break;
            }

            atoms.push_back(
              Atom(
                  Z,
                  x,
                  y,
                  z
              )  
            );
        }
    }
public:
    FileReader(std::string fileName): fileName(fileName) {
        input = new std::ifstream(fileName);

        readNumberOfAtoms();
        readAtoms();

    }

    ~FileReader() {
        input->close();
    }

    int getNumberOfAtoms() {
        return this->numberOfAtoms;
    }

    std::vector<Atom>& getAtoms() {
        return this->atoms;
    }
};